open Data;

[@react.component]
let make = (~category: category, ~lang: language) => {
  let catId = category.id;
  let handleClick = _ => ReasonReactRouter.push({j|/categories/$catId|j});
  <button onClick=handleClick className=ThemeBase.categoryButton>
    {React.string(categoryLabel(lang, category))}
  </button>;
};
