open Data;

type action =
  | CategoryLoaded(categoryDetial)
  | ResetLoadedCategory;

type state = {category: Js.Option.t(categoryDetial)};

let renderImageGroup = (label: string, images: array(image), key: string) => {
  <div className=ThemeBase.imageGroup key>
    <h2 className=ThemeBase.imageGroupCaption> {React.string(label)} </h2>
    <div className=ThemeBase.imageGroupImagesContainer>
      {images |> Array.map(img => <Image src={img.url} thumbnail={img.thumbnailUrl} key={img.id} />) |> React.array}
    </div>
  </div>;
};

[@react.component]
let make = (~categoryId) => {
  let (state, dispatch) =
    React.useReducer(
      (_state, action) =>
        switch (action) {
        | CategoryLoaded(cat) => {category: Some(cat)}
        | ResetLoadedCategory => {category: None}
        },
      {category: None},
    );
  let loadedCategoryMatchesCurrentId =
    state.category |> Js.Option.filter((. c) => c.id == categoryId) |> Js.Option.isSome;
  if (state.category->Js.Option.isNone || !loadedCategoryMatchesCurrentId) {
    Api.(getCategory(categoryId) |> then_(resp => CategoryLoaded(resp.data) |> dispatch |> resolve) |> ignore);
  };
  let catSettings = Config.getCategorySettings(categoryId);
  let applyreverseSubcategories =
    if (catSettings.reverseSubcategories) {
      Utils.Array.rev;
    } else {
      x => x;
    };
  let applySettings = applyreverseSubcategories;
  switch (state.category) {
  | Some(c) =>
    <div>
      <h1> {React.string(categoryDetailLabel(EN, c))} </h1>
      {c.images->Utils.Array.is_empty ? React.null : renderImageGroup("", c.images, "top")}
      {c.subcategories
       |> applySettings
       |> Array.map(sc => renderImageGroup(categoryDetailLabel(EN, sc), sc.images, sc.id))
       |> ReasonReact.array}
    </div>
  | _ => <div> {React.string("Loading ...")} </div>
  };
};
