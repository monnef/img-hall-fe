open Css;

// Constants

let colorPrimary = hex("552121");
let colorSecondary = hex("5d4f6e");
let colorBackground = hex("f4f3eb");

// Helpers

let p100 = pct(100.0);

let w100 = width(p100);
let h100 = height(p100);

let sm = media("(max-width: 768px)");

//let cursorPointer = cursor(pointer);
let cursorPointer = unsafe("cursor", "pointer");

let bgPosCenter = unsafe("background-position", "center");

// Components

let imageGroup = style([margin2(em(3.), em(1.))]);
let imageGroupCaption = style([]);
let imageGroupImagesContainer = style([display(flexBox), flexWrap(wrap)]);

let image =
  style([
    margin(em(0.33)),
    width(pct(30.0)),
    paddingBottom(pct(20.0)),
    display(inlineBlock),
    position(relative),
    backgroundColor(lightgray),
    sm([width(p100), paddingBottom(pct(75.0))]),
  ]);

let imageThumbnail =
  style([
    display(inlineBlock),
    backgroundSize(cover),
    backgroundRepeat(noRepeat),
    bgPosCenter,
    position(absolute),
    top(zero),
    bottom(zero),
    left(zero),
    right(zero),
  ]);

let imageBig =
  style([w100, h100, display(inlineBlock), backgroundSize(contain), backgroundRepeat(noRepeat), bgPosCenter]);

let imageBigWrapper =
  style([
    position(fixed),
    w100,
    h100,
    left(zero),
    top(zero),
    backgroundColor(rgba(10, 10, 20, 0.95)),
    zIndex(1),
  ]);

let categoryBar = style([]);
let categoryBarCatLabel = style([display(none)]);
let categoryBarList = style([listStyle(none, inside, none), margin(zero), padding(zero)]);

let categoryButton =
  style([
    marginRight(em(1.)),
    background(none),
    border(zero, none, white),
    cursorPointer,
    fontWeight(bold),
    color(colorSecondary),
  ]);

let notificationsWrapper = style([position(absolute), right(zero), top(zero)]);
let notificationsOne = style([border(px(1), solid, black), padding(em(0.5))]);

let footer = style([textAlign(center)]);

let simpleInfoBox = style([margin(em(3.)), textAlign(center)]);

let app =
  style([
    display(flexBox),
    flexDirection(column),
    padding(em(1.)),
    color(colorPrimary),
    backgroundColor(colorBackground),
    minHeight(p100),
  ]);
let appContent = style([flexGrow(1.)]);
