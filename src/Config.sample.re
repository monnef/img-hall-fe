let apiPrefix = "http://localhost:8080";

module MapString = Belt.Map.String;

type categorySettings = {reverseSubcategories: bool};

let defaultCategorySettings: categorySettings = {reverseSubcategories: false};

let categorySettingsMap: MapString.t(categorySettings) =
  [|("xxx", {reverseSubcategories: true})|]->MapString.fromArray;

let getCategorySettings = (id: string) =>
  categorySettingsMap->Belt.Map.String.getWithDefault(id, defaultCategorySettings);

let _ = Js.logMany([|"Config:", "apiPrefix =", apiPrefix|]);
