type language =
  | EN
  | CS;

let decodeLanguage = json =>
  Json.Decode.(
    string(json)
    |> (
      fun
      | "EN" => EN
      | "CS" => CS
      | _ => failwith("invalid language")
    )
  );
let encodeLanguage =
  fun
  | EN => "EN"
  | CS => "CS";

type translatedText = Js_dict.t(string);

let decodeTranslatedText = json => Json.Decode.(json |> dict(string));

type accessLevelT =
  | Public
  | Protected
  | Private;

let decodeAccessLevelT = json =>
  Json.Decode.(
    string(json)
    |> (
      fun
      | "Public" => Public
      | "Protected" => Protected
      | "Private" => Private
      | _ => failwith("invalid access level")
    )
  );

type category = {
  accessLevel: accessLevelT,
  id: string,
  label: translatedText,
  subcategories: array(category),
};

let rec decodeCategory = json =>
  Json.Decode.{
    accessLevel: json |> field("accessLevel", decodeAccessLevelT),
    id: json |> field("id", string),
    label: json |> field("label", decodeTranslatedText),
    subcategories: json |> field("subcategories", array(decodeCategory)),
  };

let decodeCategories = json => Json.Decode.(json |> array(decodeCategory));

type image = {
  id: string,
  url: string,
  date: string,
  thumbnailUrl: string,
  thumbnailId: string,
};

let decodeImage = json =>
  Json.Decode.{
    id: json |> field("id", string),
    url: json |> field("url", string),
    date: json |> field("date", string),
    thumbnailUrl: json |> field("thumbnailUrl", string),
    thumbnailId: json |> field("thumbnailId", string),
  };

type categoryDetial = {
  accessLevel: accessLevelT,
  id: string,
  label: translatedText,
  subcategories: array(categoryDetial),
  images: array(image),
};

let rec decodeCategoryDetail = json =>
  Json.Decode.{
    accessLevel: json |> field("accessLevel", decodeAccessLevelT),
    id: json |> field("id", string),
    label: json |> field("label", decodeTranslatedText),
    subcategories: json |> field("subcategories", array(decodeCategoryDetail)),
    images: json |> field("images", array(decodeImage)),
  };

let categoryLabel = (lang: language, cat: category) =>
  cat.label->Js_dict.get(lang |> encodeLanguage) |> Js_option.getWithDefault("???");

let categoryDetailLabel = (lang: language, cat: categoryDetial) =>
  cat.label->Js_dict.get(lang |> encodeLanguage) |> Js_option.getWithDefault("???");
