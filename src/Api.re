open Axios;
open Axios_types;
include Js.Promise;

let inst = Instance.create(makeConfig(~baseURL=Config.apiPrefix, ()));

type wrappedResponse('a, 'b, 'decData) = {
  response: response('a, 'b),
  data: 'decData,
};

let decodeAndWrap = (decoder: Js.Json.t => 'decData, resp: response('a, 'b)): wrappedResponse('a, 'b, 'decData) => {
  response: resp,
  data: decoder(resp##data),
};

let decodeAndWrapP =
    (decoder: Js.Json.t => 'decData, resp: response('a, 'b)): Js.Promise.t(wrappedResponse('a, 'b, 'decData)) => {
  resolve(decodeAndWrap(decoder, resp));
};

type cresp('a, 'b, 'decData) = Js.Promise.t(wrappedResponse('a, 'b, 'decData));

let getCategories = (): cresp(_, _, array(Data.category)) =>
  inst->Instance.get("/categories") |> then_(decodeAndWrapP(Data.decodeCategories));

let getCategory = (id: string): cresp(_, _, Data.categoryDetial) =>
  inst->Instance.get("/categories/" ++ id) |> then_(decodeAndWrapP(Data.decodeCategoryDetail));
