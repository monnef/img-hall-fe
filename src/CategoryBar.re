open Data;

[@react.component]
let make = (~categories: array(category), ~lang: language) => {
  let renderCat = (cat: category) => {
    let label = categoryLabel(lang, cat);
    <li key={cat.id}>
      <span className=ThemeBase.categoryBarCatLabel> {React.string(label ++ ": ")} </span>
      {cat.subcategories |> Array.map(c => <CategoryButton key={c.id} category=c lang />) |> React.array}
    </li>;
  };
  <div className=ThemeBase.categoryBar>
    <ul className=ThemeBase.categoryBarList> {categories |> Array.map(renderCat) |> React.array} </ul>
  </div>;
};
