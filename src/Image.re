type state = {
  showDetail: bool,
  isOrWasVisible: bool,
};

type action =
  | ShowDetail
  | HideDetail
  | FlagIsOrWasVisible;

[@react.component]
let make = (~thumbnail, ~src) => {
  let (isElVisible, ref) = ReactIsVisible.useIsVisible();
  let (state, dispatch) =
    React.useReducer(
      (state, action) =>
        switch (action) {
        | ShowDetail => {...state, showDetail: true}
        | HideDetail => {...state, showDetail: false}
        | FlagIsOrWasVisible => {...state, isOrWasVisible: true}
        },
      {showDetail: false, isOrWasVisible: false},
    );
  if (isElVisible && !state.isOrWasVisible) {
    dispatch(FlagIsOrWasVisible);
  };
  let thumbStyle =
    state.isOrWasVisible
      ? ReactDOMRe.Style.make(~backgroundImage={j|url($thumbnail)|j}, ()) : ReactDOMRe.Style.make();
  let thumbEl = <span className=ThemeBase.imageThumbnail style=thumbStyle onClick={_ => dispatch(ShowDetail)} ref />;
  let detail =
    <div className=ThemeBase.imageBigWrapper>
      <div
        src
        onClick={_ => dispatch(HideDetail)}
        className=ThemeBase.imageBig
        style={ReactDOMRe.Style.make(~backgroundImage={j|url($src)|j}, ())}
      />
    </div>;
  let imgEl = state.showDetail ? detail : ReasonReact.null;
  <span className=ThemeBase.image> thumbEl imgEl </span>;
};
