open Data;

let simpleInfoBox = text => <div className=ThemeBase.simpleInfoBox> {React.string(text)} </div>;

type error = {
  title: string,
  text: string,
};

type state = {
  lang: language,
  categories: Js.Option.t(array(category)),
  errors: array(error),
  categoryFetchIssued: bool,
};

type action =
  | CategoriesLoaded(array(category))
  | AddError(error)
  | CategoriesStartedLoading;

[@react.component]
let make = () => {
  let (state, dispatch) =
    React.useReducer(
      (state, action) =>
        switch (action) {
        | CategoriesLoaded(categories) => {...state, categories: Some(categories)}
        | AddError(err) => {...state, errors: Array.append(state.errors, [|err|])}
        | CategoriesStartedLoading => {...state, categoryFetchIssued: true}
        },
      {lang: EN, categories: None, errors: [||], categoryFetchIssued: false},
    );
  let url = ReasonReactRouter.useUrl();
  let content =
    switch (url.path) {
    | ["categories", id] => <CategoryScreen categoryId=id />
    | [] => simpleInfoBox("No category selected, please click on a category at the top.")
    | _ => simpleInfoBox("URL not found.")
    };
  let showError = (~text, ~title="Error", ()): unit => dispatch(AddError({title, text}));
  if (!state.categoryFetchIssued) {
    dispatch(CategoriesStartedLoading);
    Api.(
      getCategories()
      |> then_(resp => resolve(dispatch(CategoriesLoaded(resp.data))))
      |> catch(err => {
           let errMsg: string = [%raw "err.message"];
           Js.Console.error3("categories error:", errMsg, err);
           showError(~text="Request failed: " ++ errMsg, ~title="Categories", ());
           resolve();
         })
      |> ignore
    );
  };
  let errorsComp =
    <div className=ThemeBase.notificationsWrapper>
      {state.errors
       ->Belt.Array.mapWithIndex((i, e) =>
           <div key={string_of_int(i)} className=ThemeBase.notificationsOne>
             <strong> {React.string(e.title)} </strong>
             <div> {React.string(e.text)} </div>
           </div>
         )
       |> React.array}
    </div>;
  let footer =
    <div className=ThemeBase.footer> {React.string({js|Made with ❤ in Haskell and Reason by monnef.|js})} </div>;
  let debugState =
    <div style={ReactDOMRe.Style.make(~display="none", ())}>
      {React.string("State = " ++ (Js.Json.stringifyAny(state) |> Js.Option.getWithDefault("???")))}
    </div>;
  let res =
    <div className=ThemeBase.app>
      errorsComp
      <CategoryBar categories={state.categories |> Js.Option.getWithDefault([||])} lang={state.lang} />
      <div className=ThemeBase.appContent> content </div>
      <br />
      footer
      debugState
    </div>;
  res;
};
