module Array = {
  let rev = xs => xs->Array.to_list->List.rev->Array.of_list;
  let is_empty = xs => xs->Array.length == 0;
};
